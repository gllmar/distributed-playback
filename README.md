# distributed-playback

deprecated

use dm-rp-player or dm-main-dispatch instead

## Run 

qjackctl
a2jmidi_bridge
pure data
reaper


## Sync Media DB
```
scripts/sync_media_db.sh
```

## install to fresh raspberry 

### install OS Raspberry pi OS lite

### enable ssh
touch ssh on /boot

### ssh in 

### update 

```
sudo apt-get update && sudo apt-get upgrade
```

### raspi-config

```
sudo raspi-config
```

* wireless lan contry
* password
* hostname -> pi-x
* console autologin pi

```
sudo reboot
```

#### fix locales

https://gist.github.com/okaufmann/79acf73fd9f41b0b189cc3c8c480eaf5

```
echo "LC_ALL=en_US.UTF-8" | sudo tee -a /etc/environment > /dev/null
echo "en_US.UTF-8 UTF-8" | sudo tee -a /etc/locale.gen > /dev/null
echo "LANG=en_US.UTF-8" | sudo tee /etc/locale.conf > /dev/null
sudo locale-gen en_US.UTF-8
```

#### Fix fan curve

https://www.jeffgeerling.com/blog/2021/taking-control-pi-poe-hats-overly-aggressive-fan

sudo nano /boot/config.txt

```
# PoE Hat Fan Speeds
dtparam=poe_fan_temp0=50000
dtparam=poe_fan_temp1=60000
dtparam=poe_fan_temp2=70000
dtparam=poe_fan_temp3=80000
```


### get dependencies

```
sudo apt-get install git tmux
```

### manage keys

#### get devkey

#### tweak ssh key

### get repositories

mkdir src
cd src 


#### piqoli
```
git clone git@gitlab.com:gllmar/piqoli.git
    micro
    ssh
    samba-home

```

```
/etc/samba/smb.conf
##### samba


[global]
    workgroup = WORKGROUP
    encrypt passwords = yes
    wins support = yes
    log level = 1
    max log size = 1000
    read only = no

[homes]
    comment = Home Directories
    browseable = yes
    valid users = %S


```

#### pi_hello_video

cd ~/src
git clone git@github.com:gllmAR/pi_hello_video.git
    git clone https://github.com/adafruit/pi_hello_video.git

cd pi_hello_video

./rebuild.sh
cd hello_video
sudo make install

#### puredata 

cd ~/src
git clone https://github.com/pure-data/pure-data
cd pure-data
sudo apt-get install autoconf libtool libasound2-dev

sudo apt install build-essential autoconf automake libtool gettext git libasound2-dev libjack-jackd2-dev libfftw3-3 libfftw3-dev tcl tk


./autogen.sh

./configure --enable-jack --enable-fftw

make -j4
sudo make install


#### distributed-playback

```

cd ~/src 

git clone git@gitlab.com:gllmar/distributed-playback.git

>gerer le service 

cd ..

```


### tweak fstab

sudo mkdir  /mnt/nomade/
sudo mkdir  /mnt/nomade/projets

sudo micro /etc/fstab

 //nomade.local/projets  /mnt/nomade/projets  cifs  username=myname,password=123  0  0


### Copy media

mkdir ~/media && mkdir ~/media/dm

rsync -r --progress /mnt/nomade/projets/sabrina/distributed_memories/h264/ ~/media/dm


###  zita bridge?

debian
```
sudo apt-get install zita-njbridge
```


arch?

```
sudo pacman -Sy zita-njbridge
```

